package com.example.tp2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RowAdapter extends ArrayAdapter<Wine> {

    public RowAdapter(Context context, Wine[] wines) {
        super(context, 0, wines);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Wine wine = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
        }

        TextView title = convertView.findViewById(R.id.title);
        TextView region = convertView.findViewById(R.id.region);

        title.setText(wine.getTitle());
        region.setText(wine.getRegion());

        return convertView;
    }
}