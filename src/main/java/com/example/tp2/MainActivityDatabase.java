package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivityDatabase extends AppCompatActivity {

    WineDbHelper wDbH;
    SimpleCursorAdapter adapter;
    ListView wineList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wDbH = new WineDbHelper(this);
        SQLiteDatabase db = wDbH.getReadableDatabase();
        wDbH.populate();
        final Cursor cursor = wDbH.fetchAllWines();

        adapter = new SimpleCursorAdapter(this,
                R.layout.row,
                cursor,
                new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_LOC},
                new int[] {R.id.title, R.id.region});

        wineList = findViewById(R.id.wineList);
        wineList.setAdapter(adapter);
        db.close();

        wineList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor currentWineCursor = (Cursor) parent.getItemAtPosition(position);

                Wine currentWine = WineDbHelper.cursorToWine(currentWineCursor);

                Intent intent = new Intent(MainActivityDatabase.this, WineActivity.class);
                intent.putExtra("wine", currentWine);
                intent.putExtra("isNewWine", false);
                startActivityForResult(intent, 1);
            }
        });

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // 'Add a new wine' button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wine wine = new Wine();

                Intent intent = new Intent(MainActivityDatabase.this, WineActivity.class);
                intent.putExtra("wine", wine);
                intent.putExtra("isNewWine", true);
                startActivityForResult(intent, 1);
            }
        });

        // Delete a wine
        registerForContextMenu(wineList);
        wineList.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MainActivityDatabase.super.onCreateContextMenu(menu, v, menuInfo);
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.delete_wine, menu);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            Wine wine = data.getParcelableExtra("wine");
            assert wine != null;
            boolean isNewWine = data.getExtras().getBoolean("isNewWine");

            if (isNewWine) {
                boolean insert = wDbH.addWine(wine);

                if (insert) {
                    updateListAndCursor("Un nouveau vin a été ajouté.");
                }
                else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivityDatabase.this);
                    alertDialogBuilder.setTitle("Ajout impossible");
                    alertDialogBuilder.setMessage("Un vin portant le même nom et provenant de la même région existe déjà dans la base de données.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }
            }
            else {
                int res = wDbH.updateWine(wine);

                if (res == -1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivityDatabase.this);
                    alertDialogBuilder.setTitle("Modification impossible");
                    alertDialogBuilder.setMessage("Un vin portant le même nom et provenant de la même région existe déjà dans la base de données.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }
                else {
                    updateListAndCursor("Le vin a été mis à jour.");
                }
            }
        }
    }

    void updateListAndCursor(String toastMessage) {
        // Update the cursor
        adapter.changeCursor(wDbH.getReadableDatabase().query(WineDbHelper.TABLE_NAME, null, null, null, null, null, null));

        // Refresh the list of wines
        adapter.notifyDataSetChanged();

        // Toast (little message) to inform the user
        if (!toastMessage.isEmpty()) {
            Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getItemId() == R.id.delete) {
            Cursor cursor = (Cursor) wineList.getItemAtPosition(info.position);
            wDbH.deleteWine(cursor);

            updateListAndCursor("Le vin a été supprimé.");

            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
