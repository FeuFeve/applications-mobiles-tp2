package com.example.tp2;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // STEP 1
        final ListView wineList = findViewById(R.id.wineList);

        Wine[] wines = {
                new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"),
                new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"),
                new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"),
                new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"),
                new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"),
                new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"),
                new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"),
                new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"),
                new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"),
                new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"),
                new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450")
        };
        final RowAdapter adapter = new RowAdapter(getApplicationContext(), wines);

        wineList.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
