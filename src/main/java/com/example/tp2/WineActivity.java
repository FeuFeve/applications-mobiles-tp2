package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        // Get intent
        Intent intent = getIntent();
        final Wine wine = intent.getParcelableExtra("wine");
        assert wine != null;
        final boolean isNewWine = intent.getExtras().getBoolean("isNewWine");

        // Get layout elements
        final EditText wineName = findViewById(R.id.wineName);
        final EditText editWineRegion = findViewById(R.id.editWineRegion);
        final EditText editLoc = findViewById(R.id.editLoc);
        final EditText editClimate = findViewById(R.id.editClimate);
        final EditText editPlantedArea = findViewById(R.id.editPlantedArea);

        // Get content
        String title = wine.getTitle();
        String region = wine.getRegion();
        String localization = wine.getLocalization();
        String climate = wine.getClimate();
        String plantedArea = wine.getPlantedArea();

        // Set the content to the layout
        wineName.setText(title);
        editWineRegion.setText(region);
        editLoc.setText(localization);
        editClimate.setText(climate);
        editPlantedArea.setText(plantedArea);

        // Save button
        final Button saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = wineName.getText().toString();
                String region = editWineRegion.getText().toString();
                String localization = editLoc.getText().toString();
                String climate = editClimate.getText().toString();
                String plantedArea = editPlantedArea.getText().toString();

                if (title.isEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                    alertDialogBuilder.setTitle("Sauvegarde impossible");
                    alertDialogBuilder.setMessage("Le nom du vin doit être non vide.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }
                else {
                    // The name is valid
                    wine.update(title, region, localization, climate, plantedArea);

                    // Send a new intent back to update the database
                    Intent intent = new Intent(WineActivity.this, MainActivityDatabase.class);
                    intent.putExtra("wine", wine);

                    // Send the "isNewWine" boolean back to differentiate the need to call an "insert" or an "update" call to the database
                    intent.putExtra("isNewWine", isNewWine);

                    setResult(1, intent);
                    finish();
                }
            }
        });

    }
}